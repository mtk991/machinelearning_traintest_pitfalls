import sklearn
from sklearn import datasets
from matplotlib import pyplot as plt
import seaborn as sns
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
from sklearn.model_selection import train_test_split

from tensorflow import keras
from tensorflow.keras import layers

def create_model():
    inp = keras.Input(shape=(2,))
    x = keras.layers.Dense(30, activation='relu')(inp)
    x = keras.layers.Dense(30, activation='relu')(x)

    x = keras.layers.Dense(1, activation='linear')(x)

    model = keras.Model(inputs=inp, outputs=x)
    adm = keras.optimizers.Adam()
    model.compile(optimizer=adm, loss='mse')
    return model

def train_model(model,x_train,y_train,x_val,y_val):
    early_stop = keras.callbacks.EarlyStopping(monitor='val_loss', min_delta=0, patience=3, verbose=0, mode='auto')

    history = model.fit(x_train, y_train, validation_data=(x_val, y_val), shuffle=True, epochs=100,
                        callbacks=[early_stop])
    return history,model

def plot_history(hist):
    #directly borrowed from: https://machinelearningmastery.com/display-deep-learning-model-training-history-in-keras/
    plt.plot(hist.history['loss'])
    plt.plot(hist.history['val_loss'])
    plt.title('model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    #plt.show()

x1,y1 = datasets.make_circles(n_samples=2000, factor=.5,noise=.05);
x2,y2 = datasets.make_circles(n_samples=2000, factor=.5,noise=.05);

x2 = 4*x2

y1 = y1.astype(np.float32)
y2 = y2.astype(np.float32)

y1[np.where(y1==1)]=0.05
y1[np.where(y1==0)]=0.10
y2[np.where(y2==1)]=0.20
y2[np.where(y2==0)]=0.40

y_combined = np.concatenate([y1,y2])
x_combined = np.concatenate([x1,x2])

#x_combined = np.sum(x_combined**2,axis=1)

y_truetest = y_combined[np.where(y_combined==0.40)]
x_truetest = x_combined[np.where(y_combined==0.40)]


y_val = y_combined[np.where(y_combined==0.10)]
x_val = x_combined[np.where(y_combined==0.10)]


y_train = y_combined[np.where((y_combined==0.05) | (y_combined==0.20))]
x_train = x_combined[np.where((y_combined==0.05) | (y_combined==0.20))]

x_train,x_val,y_train,y_val = train_test_split(x_combined[np.where(y_combined!=0.40)],
y_combined[np.where(y_combined!=0.40)],test_size=0.333)


model = create_model()

hist,model = train_model(model,x_train,y_train,x_val,y_val)

predict = model.predict(x_truetest)

plot_history(hist)


#sns.scatterplot(x_train[:,0],x_train[:,1])
#sns.scatterplot(x_val[:,0],x_val[:,1])
#sns.scatterplot(x_truetest[:,0],x_truetest[:,1])

#plt.legend(['train','test','truetest'])
#plt.show()
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.scatter(x_train[:,0],x_train[:,1],y_train,c='b')
ax.scatter(x_val[:,0],x_val[:,1],y_val,c='r',linewidth=8)
ax.scatter(x_truetest[:,0],x_truetest[:,1],y_truetest,c='k',linewidth=8)

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.scatter(x_truetest[:,0],x_truetest[:,1],predict,c='b')
ax.scatter(x_truetest[:,0],x_truetest[:,1],y_truetest,c='r')
plt.show()